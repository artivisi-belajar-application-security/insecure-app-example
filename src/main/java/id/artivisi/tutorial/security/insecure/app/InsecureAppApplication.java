package id.artivisi.tutorial.security.insecure.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import nz.net.ultraq.thymeleaf.layoutdialect.LayoutDialect;

@SpringBootApplication
public class InsecureAppApplication {

	@Bean
	public LayoutDialect layoutDialect() {
	return new LayoutDialect();
	}

	public static void main(String[] args) {
		SpringApplication.run(InsecureAppApplication.class, args);
	}

}

package id.artivisi.tutorial.security.insecure.app.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity @Table(name = "s_user") @Data
public class User {
    @Id private String id;

    @NotNull @NotEmpty
    private String username;

    @NotNull @NotEmpty
    private String password;

    @NotNull
    private Boolean enabled = true;
}

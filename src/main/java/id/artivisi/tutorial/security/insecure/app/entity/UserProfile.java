package id.artivisi.tutorial.security.insecure.app.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity @Table(name = "s_user_profile") @Data
public class UserProfile {
    @Id private String id;

    @ManyToOne @JoinColumn(name = "id_user")
    private User user;

    @NotNull @NotEmpty
    private String fullname;
    
    @NotNull @NotEmpty
    private String address;
}

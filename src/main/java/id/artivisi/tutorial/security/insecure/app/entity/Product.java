package id.artivisi.tutorial.security.insecure.app.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity @Data
public class Product {
    @Id private String id;

    @NotNull @NotEmpty
    private String code;

    @NotNull @NotEmpty
    private String name;

    @NotNull @Min(1)
    private BigDecimal price;
}

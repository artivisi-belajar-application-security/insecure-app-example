package id.artivisi.tutorial.security.insecure.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserProfileController {

    @GetMapping("/profile/show")
    public ModelMap showUserProfile(){
        return new ModelMap();
    }
}

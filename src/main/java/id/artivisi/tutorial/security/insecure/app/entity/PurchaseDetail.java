package id.artivisi.tutorial.security.insecure.app.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;


@Entity @Data
public class PurchaseDetail {
    @Id private String id;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_purchase")
    private Purchase purchase;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_product")
    private Product product;

    @NotNull @Min(1)
    private Integer quantity = 1;
}

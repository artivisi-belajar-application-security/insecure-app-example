package id.artivisi.tutorial.security.insecure.app.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity @Data
public class Purchase {
    @Id private String id;

    @NotNull
    private LocalDateTime transactionTime;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_user")
    private User user;

    @OneToMany(mappedBy = "purchase")
    private List<PurchaseDetail> purchaseDetails = new ArrayList<>();
}

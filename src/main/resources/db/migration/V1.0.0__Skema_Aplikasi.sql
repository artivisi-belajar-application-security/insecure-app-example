create table s_role (
    id varchar(36), 
    name varchar(100) not null,
    primary key (id),
    unique (name)
);

create table s_permission (
    id varchar(36), 
    value varchar(100) not null,
    label varchar(100) not null,
    primary key (id),
    unique (value)
);

create table s_role_permission (
    id_role varchar(36) not null,
    id_permission varchar(36) not null,
    primary key (id_role, id_permission),
    foreign key (id_role) references s_role,
    foreign key (id_permission) references s_permission
);

create table s_user (
    id varchar(36),
    username varchar(100) not null,
    password varchar(100) not null,
    enabled boolean not null,
    id_role varchar(36) not null,
    unique (username), 
    primary key (id),
    foreign key (id_role) references s_role
);

create table s_user_profile (
    id varchar(36),
    id_user varchar(36) not null,
    fullname varchar(100) not null,
    address text not null,
    unique (id_user), 
    primary key (id),
    foreign key (id_user) references s_user
);

create table product (
    id varchar(36),
    code varchar(20) not null,
    name varchar(100) not null,
    price decimal(19,2) not null,
    unique (code),
    primary key (id)
);

create table purchase (
    id varchar(36), 
    id_user varchar(36) not null,
    transaction_time timestamp not null,
    primary key (id),
    foreign key (id_user) references s_user
);

create table purchase_detail (
    id varchar(36), 
    id_purchase varchar(36) not null,
    id_product varchar(36) not null,
    quantity int not null,
    primary key (id),
    foreign key (id_purchase) references purchase,
    foreign key (id_product) references product
);
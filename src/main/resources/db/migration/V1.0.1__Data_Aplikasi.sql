insert into s_role (id, name) values ('r01', 'Seller');
insert into s_role (id, name) values ('r02', 'Buyer');

insert into s_permission (id, value, label)
values ('p01', 'VIEW_ALL_PURCHASES', 'VIEW_ALL_PURCHASES');
insert into s_permission (id, value, label)
values ('p02', 'VIEW_ALL_BUYERS', 'VIEW_ALL_BUYERS');
insert into s_permission (id, value, label)
values ('p03', 'VIEW_ALL_PRODUCTS', 'VIEW_ALL_PRODUCTS');
insert into s_permission (id, value, label)
values ('p04', 'VIEW_BUYER_PURCHASES', 'VIEW_BUYER_PURCHASES');

insert into s_role_permission values ('r01', 'p01');
insert into s_role_permission values ('r01', 'p02');
insert into s_role_permission values ('r01', 'p03');
insert into s_role_permission values ('r02', 'p03');
insert into s_role_permission values ('r02', 'p04');

insert into s_user (id, username, id_role, password, enabled)
values ('u01', 'seller', 'r01', 'admin', true);
insert into s_user (id, username, id_role, password, enabled)
values ('u02', 'buyer1', 'r02', 'test123', true);
insert into s_user (id, username, id_role, password, enabled)
values ('u03', 'buyer2', 'r02', 'hello', true);

insert into s_user_profile(id, id_user, fullname, address)
values ('u01', 'u01', 'User 001', 'Jakarta Timur');
insert into s_user_profile(id, id_user, fullname, address)
values ('u02', 'u02', 'User 002', 'Bogor');
insert into s_user_profile(id, id_user, fullname, address)
values ('u03', 'u03', 'User 003', 'Bandung');

insert into product(id, code, name, price)
values ('pt001','P-001', 'Product 001', 100001);
insert into product(id, code, name, price)
values ('pt002','P-002', 'Product 002', 100002);
insert into product(id, code, name, price)
values ('pt003','P-003', 'Product 003', 100003);

insert into purchase (id, transaction_time, id_user)
values ('pr100', '2022-01-01 08:00:00', 'u02');
insert into purchase (id, transaction_time, id_user)
values ('pr101', '2022-01-02 08:00:00', 'u02');
insert into purchase (id, transaction_time, id_user)
values ('pr102', '2022-01-03 08:00:00', 'u02');
insert into purchase (id, transaction_time, id_user)
values ('pr200', '2022-02-01 18:00:00', 'u03');
insert into purchase (id, transaction_time, id_user)
values ('pr201', '2022-02-02 18:00:00', 'u03');
insert into purchase (id, transaction_time, id_user)
values ('pr202', '2022-02-03 18:00:00', 'u03');

insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd1000', 'pr100', 'pt001', 1);
insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd1010', 'pr101', 'pt002', 2);
insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd1011', 'pr101', 'pt003', 1);
insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd1020', 'pr102', 'pt002', 1);
insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd2000', 'pr200', 'pt001', 3);
insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd2010', 'pr201', 'pt002', 3);
insert into purchase_detail (id, id_purchase, id_product, quantity)
values ('pd2020', 'pr202', 'pt003', 3);
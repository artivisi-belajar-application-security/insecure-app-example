# Insecure App #

Aplikasi yang diimplementasikan secara **kurang secure**. Beberapa vulnerability yang ada di aplikasi ini:

1. A01:2021 - Broken Access Control

    * Mengakses halaman admin dengan user level operator
    * Mengakses halaman profile user lain
    * Mengakses API dari Origin yang tidak terdaftar

2. A02:2021 – Cryptographic Failures

    * Menggunakan algoritma hash password yang salah

3. A03:2021 - Injection

    * SQL Injection
    * Script Injection

4. A04:2021-Insecure Design

    * Forgot password flow
    * Rate limiting

5. A05:2021-Security Misconfiguration

    * Default password
    * Cloud service credential

6. A06:2021-Vulnerable and Outdated Components

    * Menggunakan versi SpringShell

7. A07:2021-Identification and Authentication Failures

    * Credential stuffing
    * Weak Password

8. A08:2021-Software and Data Integrity Failures

    * TBD

9. A09:2021-Security Logging and Monitoring Failures

    * Audit log
    * Data versioning

10. A10:2021-Server-Side Request Forgery

    * Forwarding user request to Backend access